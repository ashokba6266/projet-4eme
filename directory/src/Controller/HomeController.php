<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="home_")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(ProductRepository $productRepository, UserRepository $userRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'products' => $productRepository->findBy([], ['createdAt' => 'DESC'], 3),
            'users' => $userRepository->findBy([], ['createdAt' => 'DESC'], 3),
        ]);
    }
}
