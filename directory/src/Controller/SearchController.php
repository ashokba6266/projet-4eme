<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/search", name="search_")
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, ProductRepository $productRepository): Response
    {
        $products = $productRepository->findProductsByString($request->get('q'));
        if(!$products) {
            $result['products']['error'] = "No product was founded";
        } else {
            $result['entities'] = $this->getRealProducts($products);
        }

        return $this->json($result);
    }

    /**
     * @param $products
     * @return array
     */
    public function getRealProducts($products): array
    {
        foreach ($products as $product){
            $realProducts[$product->getId()] = $product->getName();
        }

        return $realProducts ?? [];
    }
}
